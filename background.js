function openPage() {
  browser.tabs.create({
    url: "my-background.html"
  });
}

browser.browserAction.onClicked.addListener(openPage);